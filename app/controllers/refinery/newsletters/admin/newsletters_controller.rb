module Refinery
  module Newsletters
    module Admin
      class NewslettersController < ::Refinery::AdminController

        crudify :'refinery/newsletters/newsletter', :xhr_paging => true

      end
    end
  end
end
