module Refinery
  module Newsletters
    class Newsletter < Refinery::Core::BaseModel
      self.table_name = 'refinery_newsletters'

      attr_accessible :title, :publish_date, :body, :photo_id, :position

      acts_as_indexed :fields => [:title, :body]

      validates :title, :presence => true, :uniqueness => true

      belongs_to :photo, :class_name => '::Refinery::Image'
    end
  end
end
