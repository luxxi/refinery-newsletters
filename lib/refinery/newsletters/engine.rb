module Refinery
  module Newsletters
    class Engine < Rails::Engine
      include Refinery::Engine
      isolate_namespace Refinery::Newsletters

      engine_name :refinery_newsletters

      initializer "register refinerycms_newsletters plugin" do
        Refinery::Plugin.register do |plugin|
          plugin.name = "newsletters"
          plugin.url = proc { Refinery::Core::Engine.routes.url_helpers.newsletters_admin_newsletters_path }
          plugin.pathname = root
          plugin.activity = {
            :class_name => :'refinery/newsletters/newsletter'
          }
          
        end
      end

      config.after_initialize do
        Refinery.register_extension(Refinery::Newsletters)
      end
    end
  end
end
