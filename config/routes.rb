Refinery::Core::Engine.routes.append do

  # Frontend routes
  namespace :newsletters do
    resources :newsletters, :path => '', :only => [:index, :show]
  end

  # Admin routes
  namespace :newsletters, :path => '' do
    namespace :admin, :path => 'refinery' do
      resources :newsletters, :except => :show do
        collection do
          post :update_positions
        end
      end
    end
  end

end
