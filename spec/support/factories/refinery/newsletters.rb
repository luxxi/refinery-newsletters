
FactoryGirl.define do
  factory :newsletter, :class => Refinery::Newsletters::Newsletter do
    sequence(:title) { |n| "refinery#{n}" }
  end
end

